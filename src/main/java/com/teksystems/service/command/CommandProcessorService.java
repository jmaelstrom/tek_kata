package com.teksystems.service.command;

public interface CommandProcessorService {
	public void run();
}
