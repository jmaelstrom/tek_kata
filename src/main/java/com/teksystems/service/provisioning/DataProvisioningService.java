package com.teksystems.service.provisioning;

public interface DataProvisioningService {

	public void provision();
}
