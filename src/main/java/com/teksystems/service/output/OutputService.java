package com.teksystems.service.output;

public interface OutputService {

	public void sendMessage(String message);
}
